# FBP Dockerfile

Dockerfile for creating an environment for the FBP UKB pipeline.

Instructions:

1. Create a directory in which to perform the build:
   ```
   mkdir build && cd build
   ```
2. Copy the Dockerfile and install.r from this repository:
   ```
   curl https://git.fmrib.ox.ac.uk/paulmc/fbp-dockerfile/-/raw/main/Dockerfile?inline=false > Dockerfile
   curl https://git.fmrib.ox.ac.uk/paulmc/fbp-dockerfile/-/raw/main/install.r?inline=false  > install.r
   ```
3. Copy private data files from `cluster1.bmrc.ox.ac.uk` (only possible with permission):
   ```
   scp -r "<username>@cluster1.bmrc.ox.ac.uk:/well/win/projects/ukbiobank/fbp/DOCKER_fidel/DATA_private.tar.gz" .
   ```
4. Build the image:
   ```
   docker build . -t fbp &> build_log.txt
   ```
5. If the image was built on a different machine than the one it is to be run on, save it to a `.tar.gz` archive:
   ```
   docker save fbp | gzip -c > fbp.tar.gz
   ```
   Then download the archive to the target machine, and load it:
   ```
   docker load < fbp.tar.gz
   ```
6. Start a container from the image:
   ```
   docker run -it fbp /bin/bash
   ``` 
